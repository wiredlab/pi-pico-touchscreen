from collections import namedtuple
import os
import time

from machine import Pin, SPI, PWM
from machine import enable_irq, disable_irq
import framebuf


_interrupt_state = None

def eint():
    enable_irq(_interrupt_state)

def dint():
    _interrupt_state = disable_irq()



Point = namedtuple("Point", ("x", "y"))


class LCD_3inch5(framebuf.FrameBuffer):
    LCD_DC   = 8
    LCD_CS   = 9
    LCD_SCK  = 10
    LCD_MOSI = 11
    LCD_MISO = 12
    LCD_BL   = 13
    LCD_RST  = 15
    TP_CS    = 16
    TP_IRQ   = 17

    def __init__(self):
        self.RED   =   0x07E0
        self.GREEN =   0x001f
        self.BLUE  =   0xf800
        self.WHITE =   0xffff
        self.BLACK =   0x0000

        self.width = 320
        self.height = 240

        self.cs = Pin(self.LCD_CS, Pin.OUT)
        self.rst = Pin(self.LCD_RST, Pin.OUT)
        self.dc = Pin(self.LCD_DC, Pin.OUT)

        self.tp_cs =Pin(self.TP_CS, Pin.OUT)
        self.irq = Pin(self.TP_IRQ, Pin.IN)

        self.cs(1)
        self.dc(1)
        self.rst(1)
        self.tp_cs(1)
        self.spi = SPI(
            1,60_000_000,
            sck=Pin(self.LCD_SCK),
            mosi=Pin(self.LCD_MOSI),
            miso=Pin(self.LCD_MISO),
        )

        self.buffer = bytearray(self.height * self.width * 2)
        super().__init__(self.buffer, self.width, self.height, framebuf.RGB565)
        self.init_display()

    def write_cmd(self, cmd):
        self.cs(1)
        self.dc(0)
        self.cs(0)
        self.spi.write(bytearray([cmd]))
        self.cs(1)

    def write_data(self, buf):
        self.cs(1)
        self.dc(1)
        self.cs(0)
        #self.spi.write(bytearray([0X00]))
        self.spi.write(bytearray([buf]))
        self.cs(1)

    def init_display(self):
        """Initialize dispaly"""
        self.rst(1)
        time.sleep_ms(5)
        self.rst(0)
        time.sleep_ms(10)
        self.rst(1)
        time.sleep_ms(5)

        self.write_cmd(0x11)
        time.sleep_ms(100)

        self.write_cmd(0x36)
        self.write_data(0x60)

        self.write_cmd(0x3a)
        self.write_data(0x55)
        self.write_cmd(0xb2)
        self.write_data(0x0c)
        self.write_data(0x0c)
        self.write_data(0x00)
        self.write_data(0x33)
        self.write_data(0x33)
        self.write_cmd(0xb7)
        self.write_data(0x35)
        self.write_cmd(0xbb)
        self.write_data(0x28)
        self.write_cmd(0xc0)
        self.write_data(0x3c)
        self.write_cmd(0xc2)
        self.write_data(0x01)
        self.write_cmd(0xc3)
        self.write_data(0x0b)
        self.write_cmd(0xc4)
        self.write_data(0x20)
        self.write_cmd(0xc6)
        self.write_data(0x0f)
        self.write_cmd(0xD0)
        self.write_data(0xa4)
        self.write_data(0xa1)
        self.write_cmd(0xe0)
        self.write_data(0xd0)
        self.write_data(0x01)
        self.write_data(0x08)
        self.write_data(0x0f)
        self.write_data(0x11)
        self.write_data(0x2a)
        self.write_data(0x36)
        self.write_data(0x55)
        self.write_data(0x44)
        self.write_data(0x3a)
        self.write_data(0x0b)
        self.write_data(0x06)
        self.write_data(0x11)
        self.write_data(0x20)
        self.write_cmd(0xe1)
        self.write_data(0xd0)
        self.write_data(0x02)
        self.write_data(0x07)
        self.write_data(0x0a)
        self.write_data(0x0b)
        self.write_data(0x18)
        self.write_data(0x34)
        self.write_data(0x43)
        self.write_data(0x4a)
        self.write_data(0x2b)
        self.write_data(0x1b)
        self.write_data(0x1c)
        self.write_data(0x22)
        self.write_data(0x1f)
        self.write_cmd(0x55)
        self.write_data(0xB0)
        self.write_cmd(0x29)

    def show_up(self):
        self.write_cmd(0x2A)
        self.write_data(0x00)
        self.write_data(0x00)
        self.write_data(0x01)
        self.write_data(0x3f)

        self.write_cmd(0x2B)
        self.write_data(0x00)
        self.write_data(0x00)
        self.write_data(0x00)
        self.write_data(0xef)

        self.write_cmd(0x2C)

        self.cs(1)
        self.dc(1)
        self.cs(0)
        self.spi.write(self.buffer)
        self.cs(1)

    def bl_ctrl(self,duty):
        pwm = PWM(Pin(self.LCD_BL))
        pwm.freq(1000)
        if(duty>=100):
            pwm.duty_u16(65535)
        else:
            pwm.duty_u16(655*duty)

    def touch_get(self):
        if self.irq() == 0:
            # reconfigure SPI for touchscreen IC
            self.spi = SPI(
                1, 5_000_000,
                sck=Pin(self.LCD_SCK),
                mosi=Pin(self.LCD_MOSI),
                miso=Pin(self.LCD_MISO)
            )
            self.tp_cs(0)

            x = 0
            y = 0
            for i in range(0,3):
                self.spi.write(bytearray([0XD0]))
                data = self.spi.read(2)
                time.sleep_us(10)
                x += ((data[0]<<8) + data[1]) >> 3

                self.spi.write(bytearray([0X90]))
                data = self.spi.read(2)
                y += ((data[0]<<8) + data[1]) >> 3
            x=x/3
            y=y/3
            point = Point(x, y)

            self.tp_cs(1)
            self.spi = SPI(
                1, 30_000_000,
                sck=Pin(self.LCD_SCK),
                mosi=Pin(self.LCD_MOSI),
                miso=Pin(self.LCD_MISO)
            )

            return point

