from collections import namedtuple


# R G B
# 5 6 5 bits
RED   =   0x07E0
GREEN =   0x001f
BLUE  =   0xf800
WHITE =   0xffff
BLACK =   0x0000


Point = namedtuple("Point", ("x", "y"))


class BarMeter:
    def __init__(self, fb,
                 position=Point(0, 0),
                 width=240,
                 height=20,
                 label='',
                 label_align='left',
                 value=0,
                 ref=0,
                 ref_width=3,
                 draw_limits=True,
                 border_color=WHITE,
                 bg_color=BLACK,
                 text_color=WHITE,
                 bar_color=GREEN,
                 limit_color=RED,
                ):
        self.fb = fb
        self.position = position
        self.width = width
        self.height = height
        self.label = label
        self.label_align = label_align
        self.value = value
        self.ref = ref
        self.ref_width = ref_width
        self.draw_limits = draw_limits
        self.border_color = border_color
        self.bg_color = bg_color
        self.text_color = text_color
        self.bar_color = bar_color
        self.limit_color = limit_color

        self.value_min = 0.0
        self.value_max = 1.0

        self.draw()

    def set_value(self, value=None):
        if value is None:
            value = self.value

        if value < self.value_min:
            value = self.value_min

        if value > self.value_max:
            value = self.value_max

        self.value = value

    def draw_border(self):
        self.fb.rect(
            self.position.x, self.position.y,
            self.width, self.height,
            self.border_color,
            False)

    def draw_limit(self, value):
        if value is None:
            return
        # left limit
        elif value <= self.value_min:
            self.fb.rect(
                self.position.x,
                self.position.y,
                3,
                self.height - 2,
                self.limit_color,
                True)
        # right limit
        elif value >= self.value_max:
            self.fb.rect(
                self.position.x + self.width - 1 - 3,
                self.position.y,
                3,
                self.height - 2,
                self.limit_color,
                True)

    def draw_bg(self):
        self.fb.rect(
            self.position.x + 1,
            self.position.y + 1,
            self.width - 2,
            self.height - 2,
            self.bg_color,
            True)

    def draw_label(self, label=None, align=None, rel_x=None, rel_y=None):
        if label is None:
            label = self.label

        if align is None:
            align = self.label_align

        # all characters are 8x8
        twidth = 8 * len(label)

        if rel_y is not None:
            y = self.position.y + rel_y
        else:
            y = self.position.y + (self.height // 2) - 4

        if rel_x is not None:
            x = self.position.x + rel_x
        else:
            x = self.position.x

        if align == 'left':
            x += 4
        elif align == 'right':
            x += self.width - twidth - 4
        elif align == 'center':
            x += (self.width - twidth) // 2
        else:
            raise Error("Unknown alignment: " + f"{align}")

        self.fb.text(label, x, y, self.text_color)

    def draw_bar(self, value=None):
        self.set_value(value)

        frac = (self.value - self.value_min) / (self.value_max - self.value_min)
        w = int(round((self.width - 1) * frac))
        self.fb.rect(
            self.position.x + 1,
            self.position.y + 1,
            w,
            self.height - 2,
            self.bar_color,
            True)

    def draw_ref(self, ref=None):
        if ref is None:
            ref = self.ref
        else:
            self.ref = ref

        if (ref >= self.value_min) and (ref <= self.value_max):
            frac = (ref - self.value_min) / (self.value_max - self.value_min)
            r = int(round((self.width - 1) * frac))
            self.fb.rect(
                self.position.x + r - self.ref_width//2,
                self.position.y + 1,
                self.ref_width,
                self.height - 2,
                self.limit_color,
                True)

    def draw(self, value=None, ref=None, limits=None):
        self.draw_border()
        self.draw_bg()
        self.draw_bar(value)
        self.draw_limit(value)
        self.draw_ref(ref)
        self.draw_label()
        if (limits is None and self.draw_limits) or limits:
            self.draw_label(f"{self.value_min:.0f}", rel_y=self.height-10, align='left')
            self.draw_label(f"{self.value_max:.0f}", rel_y=self.height-10, align='right')


class CenterBarMeter(BarMeter):
    """Center-zero meter bar."""
    def __init__(self, fb,
                 position=Point(0, 0),
                 width=240,
                 height=20,
                 label='',
                 label_align='left',
                 value=0,
                 ref=0,
                 ref_width=3,
                 draw_limits=False,
                 border_color=WHITE,
                 bg_color=BLACK,
                 text_color=WHITE,
                 bar_color=GREEN,
                 limit_color=RED,
                ):
        super().__init__(fb,
                 position=position,
                 width=width,
                 height=height,
                 label=label,
                 label_align=label_align,
                 value=value,
                 ref=ref,
                 ref_width=ref_width,
                 draw_limits=draw_limits,
                 border_color=border_color,
                 bg_color=bg_color,
                 text_color=text_color,
                 bar_color=bar_color,
                 limit_color=limit_color)
        self.value_min = -1.0
        self.value_max = +1.0

    def draw_bar(self, value=None):
        if value is None:
            return

        # update if provided
        self.set_value(value)

        frac = self.value / (self.value_max - self.value_min)
        w = int(round((self.width - 1) * abs(frac)))

        if w <= self.ref_width:
            # not visible, so no reason to continue
            return
        elif value >= 0.0:
            # from center to w
            self.fb.rect(
                self.position.x + self.width//2 - self.ref_width//2,
                self.position.y + 1,
                w,
                self.height - 2,
                self.bar_color,
                True)
        else:
            # from (center - w) to center
            self.fb.rect(
                self.position.x + self.width//2 - self.ref_width//2 - w,
                self.position.y + 1,
                w,
                self.height - 2,
                self.bar_color,
                True)


class Button:
    def __init__(self, fb,
                 position=Point(0, 0),
                 width=65,
                 height=50,
                 label='',
                 label_align='center',
                 value=0,
                 border_color=WHITE,
                 bg_color=BLACK,
                 text_color=WHITE,
                 act_color=RED,
                ):
        self.fb = fb
        self.position = position
        self.width = width
        self.height = height
        self.label = label
        self.label_align = label_align
        self.value = 0
        self.border_color = border_color
        self.bg_color = bg_color
        self.text_color = text_color
        self.act_color = act_color

        self.draw()

    def handle(self, p):
        last = self.value
        if p is None:
            self.value = 0
        elif (p.x >= self.position.x and
            p.x <= (self.position.x + self.width) and
            p.y >= (self.position.y) and
            p.y <= (self.position.y + self.height)):
            self.value = 1
        else:
            self.value = 0

        # detect edges
        # rising  +1
        # falling -1
        d = self.value - last

        self.draw(self.value)
        r = (self.value, d)
        return r

    def state(self):
        return self.value

    def draw_border(self):
        self.fb.rect(
            self.position.x, self.position.y,
            self.width, self.height,
            self.border_color,
            False)

    def draw_bg(self, active=None):
        if active is None:
            active = self.value

        # background is inside the border
        if active:
            self.fb.rect(
                self.position.x + 1,
                self.position.y + 1,
                self.width - 2,
                self.height - 2,
                self.act_color,
                True)
        else:
            self.fb.rect(
                self.position.x + 1,
                self.position.y + 1,
                self.width - 2,
                self.height - 2,
                self.bg_color,
                True)

    def draw_label(self, label=None, align=None):
        if label is None:
            label = self.label

        if align is not None:
            self.label_align = align

        # all characters are 8x8
        twidth = 8 * len(label)
        y = self.position.y + (self.height // 2) - 4
        if self.label_align == 'left':
            x = self.position.x + 4
        elif self.label_align == 'right':
            x = self.position.x + self.width - twidth - 4
        elif self.label_align == 'center':
            x = self.position.x + (self.width - twidth) // 2
        else:
            raise Error("Unknown alignment: " + f"{align}")

        self.fb.text(label, x, y, self.text_color)

    def draw(self, active=None):
        self.draw_border()
        self.draw_bg(active)
        self.draw_label()


