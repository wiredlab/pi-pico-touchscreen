# Built-in packages
import random


# MicroPython-specific packages


# Local packages
from hx711 import hx711


class Loadcell:
    def __init__(self, clock_pin, data_pin, gain, tau=1.0):
        self.gain = gain
        self.tau = tau
        self.value = 0

        self.SCALE_N = 1
        self.OFFSET = 0

    def read(self):
        self.value += self.tau * (random.getrandbits(16) - self.value)
        print(f"hx: {self.value}")
        return self.value

    def tare(self, n=15):
        x = 0
        for _ in range(n):
            x += self.read()
        x = x / n
        self.OFFSET = x
        print(f"hx-tare: {x}")
        return self.OFFSET

    @property
    def newtons(self):
        return (self.value - self.OFFSET) / self.SCALE_N

    @property
    def kg(self):
        return self.newtons / 9.80665

class LoadcellHX711(Loadcell):
    def __init__(self, clock_pin, data_pin, gain, tau=1.0):
        self.gain = gain
        self.tau = tau
        self.value = 0

        self.hx = hx711(clock_pin, data_pin)

        self.hx.set_power(hx711.power.pwr_up)
        self.hx.set_gain(hx711.gain.gain_128)
        self.hx.set_power(hx711.power.pwr_down)
        hx711.wait_power_down()
        self.hx.set_power(hx711.power.pwr_up)

        # 4. wait for readings to settle
        hx711.wait_settle(hx711.rate.rate_10)

        self.SCALE_N = 1
        self.OFFSET = 0

    def read(self):
        self.value += self.tau * (self.hx.get_value() - self.value)
        print(f"hx: {self.value}")
        return self.value

