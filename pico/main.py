# built-in packages
from collections import namedtuple
from math import exp, pi


# MicroPython-specific packages
import utime
from machine import Pin
from machine import Timer
from machine import enable_irq, disable_irq
import micropython
micropython.alloc_emergency_exception_buf(100)


# Local packages
from LCD_3inch5 import LCD_3inch5

from loadcell import Loadcell

import gui
from gui import BarMeter
from gui import CenterBarMeter
from gui import Button
from gui import Point


# Load cell scale factor
LOADCELL_SCALE_N = 1

# bar meter
BAR_MIN = 0
BAR_MAX = 300

# deviation meter
DEVIATION_MIN = -20
DEVIATION_MAX = +20

# HX711 samples at 10 Hz
SAMPLE_RATE = 2

# FILTER_CUTOFF = 0.1
FILTER_CUTOFF = 0.1

TIME_CONSTANT = 1 - exp(-2 * pi * FILTER_CUTOFF / SAMPLE_RATE)
print(TIME_CONSTANT)


HX_OUT_PIN = Pin(27, Pin.IN, pull=Pin.PULL_DOWN)
HX_SCK_PIN = Pin(26, Pin.OUT)




_interrupt_state = None

def eint():
    enable_irq(_interrupt_state)

def dint():
    _interrupt_state = disable_irq()






scale = Loadcell(HX_SCK_PIN, HX_OUT_PIN, 128, TIME_CONSTANT)
# scale = LoadcellHX711(HX_SCK_PIN, HX_OUT_PIN, 128, TIME_CONSTANT)
scale.SCALE_N = LOADCELL_SCALE_N
scale.SCALE_N = float(2**16) / 300
scale.value = 150


#
# regularly sample the force sensor
#
sampler = Timer(mode=Timer.PERIODIC,
                freq=SAMPLE_RATE,
                callback=lambda t: scale.read())


def main():
    LCD = LCD_3inch5()
    LCD.bl_ctrl(100)
    LCD.fill(gui.BLACK)
    LCD.show_up()

    METER_MAX = 300

    reference = 150

    bar = BarMeter(
        LCD,
        Point(0, 0),
        width=320,
        height=150,
        label="Force (N)",
        label_align='left',
        draw_limits=True)
    bar.value_min = BAR_MIN
    bar.value_max = BAR_MAX

    buttonSub = Button(
        LCD,
        Point(0, 150),
        width=60,
        height=240 - 150,
        label="-5",
        label_align='center')

    deviation = CenterBarMeter(
        LCD,
        Point(60, 150),
        width=320 - 60 - 60,
        height=240 - 150,
        label="",
        label_align="left",
        text_color=gui.RED,
        draw_limits=True)
    deviation.value_min = DEVIATION_MIN
    deviation.value_max = DEVIATION_MAX

    buttonAdd = Button(
        LCD,
        Point(320-60, 150),
        width=60,
        height=240 - 150,
        label="+5",
        label_align='center')

    buttonTare = Button(
        LCD,
        Point(0, 0),
        width=50,
        height=40,
        label="Tare",
        label_align='center')

    while True:
        if val := scale.newtons:
            bar.label = f"Force: {val:5.1f} N"
            bar.draw(val, reference)
            deviation.draw(val - reference)

        tp = LCD.touch_get()
        if tp is not None:
            # swap
            tp  = Point(tp.y, tp.x)
            x = 320 - int((tp.x - 430) * 320 / 3270)
            y = int((tp.y - 430) * 240 / 3270)
            tp = Point(x, y)

        btn, edge = buttonSub.handle(tp)
        if edge > 0:
            reference -= 5

        btn, edge = buttonAdd.handle(tp)
        if edge > 0:
            reference += 5

        btn, edge = buttonTare.handle(tp)
        if edge > 0:
            scale.tare()
            # print(f"hx_offset: {scale.OFFSET}")

        deviation.label = f"{reference:5.1f}"

        LCD.show_up()
        utime.sleep(0.1)


if __name__ == "__main__":
    main()

