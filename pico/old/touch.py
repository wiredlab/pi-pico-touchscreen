
from collections import namedtuple
import time

from machine import Pin
from machine import enable_irq, disable_irq


Point = namedtuple("Point", ("x", "y"))


class noIRQ(object):
    irq_state = None

    def __enter__(self):
        self.irq_state = disable_irq()

    def __exit__(self, *args):
        enable_irq(self.irq_state)

class Touch:
    def __init__(self, spi):
        self.spi = spi
        self.cs = Pin(16, Pin.OUT, value=1)
        self.irq = Pin(17, Pin.IN)

        # respond to interrupts
        self.irq.irq(
            handler=self.callback,
            trigger=Pin.IRQ_FALLING,
        )
        self.value = None

    def callback(self, pin):
        # touch event
        if pin() == 0:
            self.value = self.get()
        else:
            self.value = None

    def get(self):
        x = 0
        y = 0

        with noIRQ():
            self.cs(0)
            # read 3 times for an average??
            for i in range(0,3):
                self.spi.write(bytearray([0XD0]))
                data = self.spi.read(2)
                time.sleep_us(10)
                x += ((data[0]<<8)+data[1])>>3

                self.spi.write(bytearray([0X90]))
                data = self.spi.read(2)
                y += ((data[0]<<8)+data[1])>>3
            self.cs(1)

        x = x/3
        y = y/3

        return Point(x, y)


